# dotfiles

This repo contains all the dotfiles for my minimalist i3 setup

## Requirements

Make sure you've got these packages before installing

- picom
- alacritty
- zsh
- oh-my-zsh
- zsh-syntax-highlighting
- thunar
- firefox
- rofi
- dunst
- flameshot
- pango
- nerf-fonts
- stow
- git
- alsa-utils
- imagemagick
- keepassxc
- syncthing
- gitui
- btop
- helix-editor

## Installation

First of all, clone this repository in your home folder

```
git clone https://gitlab.com/thatscringebro/dotfiles.git
```

Then, use GNU stow to create the symlinks

```
cd dotfiles
stow .
```
